Question:

@Entity
@Table(name="Questions", schema="db2")
	@id	
	
	@ElementCollection 	//ist Liste von Objekten auch eine Collection?
	@CollectionTable(name="ANSWERS",schema ="db2")
			

	@ManyToOne
	
	

Category:

@Entity
@Table(name="Category", schema="db2")
	@id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="CategoryID")
	
	@Column(unique = true)			//für Kategorie Name
	@OneToMany(mappedBy = "category")
	
	
Answer:

@Embeddable							//weil Komposotion

	@Transient 						//igonrieren der nächsten Teile für die DB für Question question
	

Game:

@Entity
@Table(name="Game", schema="db2")
	@id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="GameId")
	
	@ManyToOne
	
	@ManyToMany
	

Player:	

@Entity
@Table(name="Player", schema="db2")
	@id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PlayerId")
	
	@OneToMany(mappedBy = "player")
	
	
	
	
	