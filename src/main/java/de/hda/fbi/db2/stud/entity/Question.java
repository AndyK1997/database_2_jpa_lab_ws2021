package de.hda.fbi.db2.stud.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Question", schema = "db2")
public class Question {

  @Id
  private int questionID;

  private String quesPhrase;

  @ElementCollection
  @CollectionTable (name = "Answer", schema = "db2")
  private List<Answer> answers;

  @ManyToOne
  private Category category;

  public Question() {}

  /**
   * Konstruktor.
   *
   * @param questionID     ID der Frage
   * @param quesPhrase String mit der Frage
   * @param answers    Liste mit den Antworten (Objekte)
   * @param category   Kategorie der Frage
   */
  public Question(int questionID, String quesPhrase, List<Answer> answers,
      Category category) {
    this.questionID = questionID;
    this.quesPhrase = quesPhrase;
    this.answers = answers;
    this.category = category;
  }

  public String getQuesPhrase() {
    return quesPhrase;
  }

  public List<Answer> getAnswers() {
    return answers;
  }

  public Category getCategory() {
    return category;
  }

  public int getQuestionID() {
    return questionID;
  }

  public void setAnswers(List<Answer> answers) {
    this.answers = answers;
  }

  /**
   * Gets the correct answer and returns it.
   *
   * @return correct answer
   */
  public Answer getSolution() {
    Answer solution = new Answer();
    for (Answer a : this.answers) {
      if (a.getIsTrue() == true) {
        solution = a;
      }
    }
    return solution;
  }

  /**
   * String mit den Antworten aus den Objekten erstellen.
   *
   * @allAnswers String mit den Antworten
   */
  public String generateAnswers() {
    String allAnswers = "";
    for (Answer a : answers) {
      allAnswers += " | " + a.getAnswer() + " | ";
    }
    return allAnswers;
  }

  @Override
  public int hashCode() {
    return Objects.hash(questionID);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Question question = (Question) obj;
    return questionID == question.questionID;
  }
}
