package de.hda.fbi.db2.stud.entity;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Game", schema = "db2")
public class Game {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "GameId")
  private int gameId;

  public Player getPlayer() {
    return player;
  }

  @ManyToOne
  private Player player;

  private Timestamp timeStampStart;
  private Timestamp timeStampEnd;


  @ElementCollection
  @CollectionTable(name = "GivenAnswer", schema = "db2")
  private Map<Question, Answer> questionGivenAnswers;

  public Map<Question, Answer> getQuestionGivenAnswers() {
    return questionGivenAnswers;
  }

  public Game() {

  }

  public Timestamp getTimeStampStart() {
    return ((Timestamp) timeStampStart.clone());
  }

  public Timestamp getTimeStampEnd() {
    return ((Timestamp) timeStampEnd.clone());
  }


  public Game(Player player, List<?> questionList) {
    this.player = player;
    questionGivenAnswers = new HashMap<>();
    for (Object q : questionList) {
      questionGivenAnswers.put((Question) q, null);
    }

  }

  public void setTimeStampStart() {
    this.timeStampStart = new Timestamp(System.currentTimeMillis());
  }

  public void setTimeStampEnd() {
    this.timeStampEnd = new Timestamp(System.currentTimeMillis());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Game)) {
      return false;
    }
    Game game = (Game) o;
    return gameId == game.gameId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(gameId);
  }
}
