package de.hda.fbi.db2.stud.entity;

import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Embeddable
public class Answer {

  private String answer;

  @Transient
  private Question question;

  private Boolean isTrue;

  /**
   * Konstruktor.
   *
   * @param answer   String mit Antwort
   * @param question ID der Frage
   */
  public Answer(String answer, Question question, Boolean isTrue) {
    this.answer = answer;
    this.question = question;
    this.isTrue = isTrue;
  }

  public Answer() {
  }

  public void setIsTrue(Boolean isTrue) {
    this.isTrue = isTrue;
  }

  public Boolean getIsTrue() {
    return isTrue;
  }

  public Question getQuestion() {
    return question;
  }

  public String getAnswer() {
    return answer;
  }

  @Override
  public int hashCode() {
    return Objects.hash(answer);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Answer answer = (Answer) obj;
    return Objects.equals(this.answer, answer.answer);
  }
}
