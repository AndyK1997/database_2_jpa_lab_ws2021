package de.hda.fbi.db2.stud.impl;

import de.hda.fbi.db2.api.Lab02EntityManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class SaveData extends Lab02EntityManager {

  public SaveData() {
  }

  @Override
  public void persistData() {
    EntityManager em = getEntityManager();
    EntityTransaction tx = null;
    try {
      tx = em.getTransaction();
      tx.begin();

      lab01Data.getCategories().forEach(c -> em.persist(c));
      lab01Data.getQuestions().forEach(q -> em.persist(q));

      tx.commit();

    } catch (RuntimeException e) {
      System.err.println(e.toString());
      if (tx != null && tx.isActive()) {
        tx.rollback();
      }
      throw e;
    } finally {
      em.close();
    }
  }

  @Override
  public EntityManager getEntityManager() {
    EntityManagerFactory emf = Persistence
        .createEntityManagerFactory("docker-local-postgresPU");
    return emf.createEntityManager();
  }
}
