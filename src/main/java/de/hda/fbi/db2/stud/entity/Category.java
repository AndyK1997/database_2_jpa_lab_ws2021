package de.hda.fbi.db2.stud.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "Category", schema = "db2")
public class Category {

  @Id
  @GeneratedValue (strategy = GenerationType.AUTO)
  @Column (name = "CategoryID")
  private int categoryID;

  @Column (unique = true)
  private String name;

  @OneToMany (mappedBy = "category")
  private List<Question> questions = new ArrayList<>();

  public Category() {}

  public Category(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public int getcID() {
    return categoryID;
  }

  public int questionCount() {
    return questions.size();
  }

  public List<Question> getQuestionList() {
    return this.questions;
  }

  @Override
  public int hashCode() {
    return Objects.hash(categoryID);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Category category = (Category) obj;
    return categoryID == category.categoryID;
  }
}
