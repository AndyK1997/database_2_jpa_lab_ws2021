package de.hda.fbi.db2.stud.impl;

import de.hda.fbi.db2.api.Lab01Data;
import de.hda.fbi.db2.stud.entity.Answer;
import de.hda.fbi.db2.stud.entity.Category;
import de.hda.fbi.db2.stud.entity.Question;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

public class ReadCsvFile extends Lab01Data {

  // Kategorien als Hashtable sammeln für schnellen Zugriff
  private Hashtable<String, Category> listOfCategories = new Hashtable<>();

  @Override
  public List<Object> getQuestions() {
    ArrayList<Question> questions = new ArrayList<>();
    // alle Questions von allen Kategorien der Question Liste hinzufügen
    listOfCategories.forEach((k, v) -> questions.addAll(v.getQuestionList()));
    return new ArrayList<>(questions);
  }

  @Override
  public List<Object> getCategories() {
    return new ArrayList<>(listOfCategories.values()); // wichtig! nur die Values aus dem Hashtable
  }

  @Override
  public void loadCsvFile(List<String[]> additionalCsvLines) {
    additionalCsvLines.remove(0); //erste Zeile mit Tabellenheader entfernen (falsche Datentypen)

    for (String[] line : additionalCsvLines) {

      // Attribute von Question erstellen, bzw. einlesen:
      int quesID = Integer.parseInt(line[0]);
      String quesPhrase = line[1];
      // Category wenn existent aus Liste holen, ansonsten neu erstellen und in Hashtable aufnehmen
      Category category = categoryCheck(line[7]);

      // Question mit Attributen von oben und leeren Answers erstellen:
      Question question = new Question(quesID, quesPhrase, null, category);

      // answers Liste füllen:
      List<Answer> answers = Arrays
          .asList(new Answer(line[2], question, false), new Answer(line[3], question, false),
              new Answer(line[4], question, false), new Answer(line[5], question, false));

      // richtige Antwort als true markieren:
      answers.get(Integer.parseInt(line[6]) - 1).setIsTrue(true);

      // gesetzte answers Liste in question setzen:
      question.setAnswers(answers);

      // Erstellte Question in Question-Liste der Category aufnehmen:
      category.getQuestionList().add(question);
    }
    // Ausgabe auf die Konsole:
    printData();
  }

  private Category categoryCheck(String categoryName) {
    Category tmp = listOfCategories.get(categoryName);  // mit Key auf Hashtable zugreifen
    if (tmp == null) { // wenn noch nicht vorhanden
      tmp = new Category(categoryName);  // neue Category erstellen
      listOfCategories.put(tmp.getName(), tmp); // Category der Hashtable hinzufügen
    }
    return tmp;
  }

  private void printData() {

    for (Category c : listOfCategories.values()) {
      List<Question> tmpQuestionListe = c.getQuestionList();
      System.out.println("\n--------------------------- Kategorie: " + c.getName()
          + " ---------------------------");

      for (Question q : tmpQuestionListe) {
        System.out.println(
            "Frage:\n" + q.getQuesPhrase() + "\n"
                + "Kategorie: " + q.getCategory().getName() + "\n"
                + "Antworten: " + q.generateAnswers() + "\n"
                + "Index der Lösung: " + printIndexOfSolution(q) + "\n");
      }
    }

    System.out.println(
        "Anzahl Fragen: " + countQuestions() + "\nAnzahl Kategorien: " + countCategories());
  }

  private int printIndexOfSolution(Question question) {
    int index = (question.getAnswers().indexOf(question.getSolution())) + 1;
    return index;
  }

  private int countQuestions() {
    int numberOfQuestions = 0;
    for (Category c : listOfCategories.values()) {  // von jeder Category
      numberOfQuestions += c.questionCount(); // die Anzahl der Fragen aufaddieren
    }
    return numberOfQuestions;
  }

  private int countCategories() {
    return listOfCategories.size();
  }
}
