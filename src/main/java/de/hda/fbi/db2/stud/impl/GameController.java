package de.hda.fbi.db2.stud.impl;

import de.hda.fbi.db2.api.Lab03Game;
import de.hda.fbi.db2.stud.entity.Answer;
import de.hda.fbi.db2.stud.entity.Category;
import de.hda.fbi.db2.stud.entity.Game;
import de.hda.fbi.db2.stud.entity.Player;
import de.hda.fbi.db2.stud.entity.Question;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class GameController extends Lab03Game {

   private List<Player> players = new ArrayList<>();
  //private List<Player> newPlayers = new ArrayList<>();
  private Boolean playerExist=false;

  @Override
  public Object getOrCreatePlayer(String playerName) {
    EntityManager em = lab02EntityManager.getEntityManager();
    EntityTransaction tx = null;
    //Player newPlayer=null;
    try {
      tx = em.getTransaction();
      tx.begin();
      players = em.createQuery("SELECT p FROM Player p").getResultList();
      tx.commit();
    } catch (Exception e) {
      System.out.println(e.toString());
    } finally {
      em.close();
    }

    for (Player player : players) {
      if (player.getName().equals(playerName)) {
        this.playerExist=true;
        return player;
      }
    }

    Player newPlayer = new Player();
    newPlayer.setName(playerName);
    players.add(newPlayer);
    this.playerExist=false;
    return newPlayer;
  }

  @Override
  public Object interactiveGetOrCreatePlayer() {
    String gamerTag;
    System.out.println("Geben sie ihren GamerTag ein");
    Scanner sc = new Scanner(System.in, StandardCharsets.UTF_8.name());
    gamerTag = sc.nextLine();
    Object player=getOrCreatePlayer(gamerTag);
    //return getOrCreatePlayer(gamerTag);
    return player;
  }

  @Override
  public List<Question> getQuestions(List<?> categories, int amountOfQuestionsForCategory) {
    List<Question> questions = new ArrayList<>();
    for (Object category : categories) {
      List<Question> q = ((Category) category).getQuestionList();
      if (q.size() > amountOfQuestionsForCategory) {
        Collections.shuffle(q);
        for (int x = 0; x < amountOfQuestionsForCategory; x++) {
          questions.add(q.get(x));
        }
      } else {
        questions.addAll(q);
      }
    }
    return questions;
  }

  @Override
  public List<?> interactiveGetQuestions() {

    Scanner sc = new Scanner(System.in, StandardCharsets.UTF_8.name());
    List<Category> categories = new ArrayList<>();
    int amountCategory;
    do {
      System.out.println("Wie viele Kategorien (mindestens 2)?");
      amountCategory = sc.nextInt();
      System.out.println(amountCategory);
      if (amountCategory > lab01Data.getCategories().size()) {
        System.out.println("Zu viele Kategorien");
      }
    } while (amountCategory < 2 && lab01Data.getCategories().size() > amountCategory);

    System.out.println("Liste der Kategorien:");
    int CatID=1;
    for (Object category : lab01Data.getCategories()) {
      Category tmp = ((Category) category);
      System.out.println("ID: " + CatID + "Name: " + tmp.getName());
      CatID++;
    }
    for (int x = 0; x < amountCategory; x++) {
      int choose;
      choose = sc.nextInt();
      Object c = lab01Data.getCategories().get(choose - 1);
      categories.add((Category) c);
    }
    int amountQuestions;

    do {
      System.out.println("Wie viele Fragen (mindetens 1)");
      amountQuestions = sc.nextInt();
    } while (amountQuestions < 1);

    return getQuestions(categories, amountQuestions);
  }

  @Override
  public Object createGame(Object player, List<?> questions) {
    Object game= new Game((Player) player,questions);
    ((Player) player).getGames().add(((Game)game));

    return game;
  }

  @Override
  public void playGame(Object game) {
    Random random = new Random();
    ((Game) game).setTimeStampStart();
    Map<Question, Answer> questions = ((Game) game).getQuestionGivenAnswers();
    for (Map.Entry<Question, Answer> quest : questions.entrySet()) {
      int index = random.nextInt(4);
      quest.setValue(quest.getKey().getAnswers().get(index));
    }
    ((Game) game).setTimeStampEnd();
  }

  @Override
  public void interactivePlayGame(Object game) {
    Map<Question, Answer> questions = ((Game) game).getQuestionGivenAnswers();

    ((Game) game).setTimeStampStart();
    Scanner sc = new Scanner(System.in, StandardCharsets.UTF_8.name());
    System.out.println("Herzlich wilkommen bei Wer Wird Millionär");
    for (Map.Entry<Question, Answer> quest : questions.entrySet()) {
      int answerIndex = 0;
      System.out.println("Frage: " + quest.getKey().getQuesPhrase());
      for (Object answer : quest.getKey().getAnswers()) {
        System.out.println(answerIndex + 1 + ": " + ((Answer) answer).getAnswer());
        answerIndex++;
      }

      int index;
      do {
        System.out.println("Geben sie den Index ihrer Antwort ein Index zwischen 1-4");
        index = sc.nextInt();
      } while (index > 4 || index == 0);
      quest.setValue(quest.getKey().getAnswers().get(index - 1));
      if (quest.getKey().getAnswers().get(index - 1).getIsTrue()) {
        System.out.println("Antwort ist korrekt");
      } else {
        System.out.println("Antwort ist falsch");
      }
    }
    ((Game) game).setTimeStampEnd();
  }

  @Override
  public void persistGame(Object game) {
    EntityManager em = lab02EntityManager.getEntityManager();
    EntityTransaction tx = null;
    try {
      tx = em.getTransaction();
      tx.begin();
      if(!this.playerExist) {
        em.persist(((Game) game).getPlayer());
      }
      em.persist(game);
      tx.commit();

    } catch (RuntimeException e) {
      System.err.println(e.toString());
      if (tx != null && tx.isActive()) {
        tx.rollback();
      }
      throw e;
    } finally {
      em.close();
    }
  }
}

